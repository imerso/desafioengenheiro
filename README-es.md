![Desafio Tatic] (logo-tatic.png =150x76 "Desafio Tatic Estágio")

## Desafio Tatic

En las compañías de telecomunicaciones, los registros de llamadas, uso de datos y mensajes se almacenan en forma de un tipo de registro llamado CDR. El CDR contiene toda la información para un evento determinado y se generan millones de registros todos los días.
Los archivos CDR se envían periódicamente por mediación y se almacenan en un directorio específico.

Desarrolle un sistema que reciba periódicamente archivos CDR y los almacene de la manera más óptima posible para optimizar el costo de la infraestructura y ofrecer un buen rendimiento.
El sistema debe proporcionar un front-end web donde pueda buscar CDR por período y por indexadores.

Las diversas áreas de estas empresas necesitan buscar los datos de manera eficiente. Algunas de las búsquedas son:

* Buscar todas las llamadas de un número en un período determinado
* Buscar todas las llamadas entrantes de un número determinado en un período determinado
* Buscar todas las llamadas que pasaron por una antena en particular en un período determinado

En el archivo files.tar.gz encontrará un listado de archivos con registros de llamadas:

```
	EXCHANGE_ID;CALLING_NUMBER;CALLED_NUMBER;SERIAL_NUMBER;CALL_TYPE;CALL_DURATION;START_TIME;IMSI;SWITCH;CELL_IN;CELL_OUT;TECNOLOGIA;FILE_NAME;FIRST_LAC;LAST_LAC;GGSN_ADDRESS
    8138;5521987366501;5521908100740;5604248321560188;1;98;2019-10-05T05:56:17;8136378640573727;39;65234;00056;A;calls.txt;f86a89;bf1043;endpoint.ggsn.tatic.com
    2082;5521900070358;5511970605214;5641340773308212;2;64;2019-10-01T07:51:32;3130005345877044;45;00072;65234;B;calls.txt;d6b5ba;2e4871;endpoint.ggsn.tatic.com
    2289;5521946272014;5521908476414;6555528327464448;1;318;2019-10-23T13:23:10;6673275331277723;80;65234;65234;A;calls.txt;691b6c;66e885;endpoint.ggsn.tatic.com
    4418;5521943062830;5511947307567;3524862623171027;2;270;2019-10-19T16:12:46;8721648346805728;22;65234;00084;B;calls.txt;92da89;b2b700;endpoint.ggsn.tatic.com
    8099;5511934503573;5531963457733;5307621106101687;1;334;2019-10-28T09:16:23;4036125477586754;22;65234;65234;A;calls.txt;ca0ad2;9c592d;endpoint.ggsn.tatic.com
    9655;5511936554323;5511928735222;0323314573232566;2;288;2019-10-11T15:56:24;0314260520317028;72;65234;00205;B;calls.txt;3f9c14;a0e75e;endpoint.ggsn.tatic.com
    6276;5521937485782;5511911857043;4015541614151871;1;223;2019-10-29T04:38:19;4670717343134843;80;60542;00056;A;calls.txt;c66229;5661af;endpoint.ggsn.tatic.com
    7956;5521947606588;5521964036206;8554087705132788;2;184;2019-10-09T05:43:14;7612136753566556;45;00072;00056;B;calls.txt;89626b;81fd37;endpoint.ggsn.tatic.com
    9372;5531956202750;5511966801063;7257362521470148;1;261;2019-10-20T00:41:27;3127846123313470;72;00084;60542;A;calls.txt;4ff8b7;15e13c;endpoint.ggsn.tatic.com
    9917;5531905325437;5531934154518;7015405572621345;2;208;2019-10-12T09:20:41;4610640064438776;72;00084;00056;B;calls.txt;da5f76;002371;endpoint.ggsn.tatic.com
    7347;5521978073827;5531984760752;1083704670055286;1;170;2019-10-03T19:18:44;5228135712634222;80;00205;00072;A;calls.txt;cd2daa;f03c48;endpoint.ggsn.tatic.com
    3146;5511984308411;5521901386104;8786088428818121;2;208;2019-10-08T07:11:02;1546862072458717;80;65234;60542;B;calls.txt;187d00;a1df40;endpoint.ggsn.tatic.com
```

Cada línea de este archivo es un CDR que contiene datos de llamadas.

# Requisitos

* Los archivos deben almacenarse en un formato estructurado específico, como AVRO, ORC o PARQUET. Si lo considera necesario, puede usar otro formato, incluso si desea almacenar los datos. Justifica tu elección.
* Debe particionar e indexar datos para búsquedas rápidas.
* Debe construir el front-end web utilizando una de las tecnologías indicadas: React, Vue.js y Angular.
* El backend que almacena los datos debe construirse usando Spring Boot y debe ejecutarse en modo *daemon*.
* Debería crear un conjunto de pruebas automatizadas.
* Cualquier tecnología distinta a las indicadas puede usarse para complementar la solución.

## Protótipo Front-end

Use el siguiente prototipo como inspiración para su interfaz.

![Prototipo](Prototipo-es.png "Protótipo")

## Recomendaciones

Las siguientes recomendaciones no son obligatorias, pero serán apreciadas si se resuelven.

* Almacene grandes volúmenes de datos
* Optimizar el uso del espacio en disco
* Almacene archivos grandes que no caben en la memoria principal
* Optimizar el uso de RAM
* Elija cómo particionar datos para facilitar la búsqueda
* Permitir búsquedas en múltiples campos, evitando explorar todos los archivos

## Calificación

Se evaluarán los siguientes criterios

* Uso eficiente del disco: el programa de almacenamiento debe preocuparse por utilizar el menor espacio posible en el disco para los datos procesados.
* Búsqueda de datos eficiente: el programa de búsqueda debe preocuparse por traer los resultados solicitados en el menor tiempo posible.
* Código de calidad (uso de buenas prácticas y adopción de buen diseño).
* Robusto almacenamiento y aplicaciones de búsqueda.
* Existencia de pruebas automatizadas.

## Entrega

* Cree un repositorio en su github privado o cuenta de bitbucket de acceso público
* El proyecto debe contener un archivo Readme.md con instrucciones de compilación y ejecución.
* Agregue secciones que justifiquen sus elecciones
* El sistema debe ejecutarse con docker y docker-compose
